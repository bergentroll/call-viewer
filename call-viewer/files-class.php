<?php

/* call-viewer
 * Copyright (C) 2018 Anton Karmanov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Iterable class that grabs and keep filenames from data directory.
class Files implements ArrayAccess, Countable {
  public function __construct($DIR, $regexp = '', $file_suffix = 'wav') {
    $files_tmp = scandir($DIR, SCANDIR_SORT_DESCENDING);
    foreach ($files_tmp as $file) {
      if (substr($file, -3) == $file_suffix && preg_match("/$regexp/", $file)) {
        $this->files[] = $file;
      }
    }
    unset($files_tmp);
  }

  public function __destruct() {
    unset($this->files);
  }

  // Method prints the array content to a web-page.
  public function debug() {
    echo "<pre>";
    print_r($this->files);
    echo "</pre>";
  }

  public function count() {
    return count($this->files);
  }

  public function offsetSet($key, $value) {
    $this->files[$key] = $value;
  }

  public function offsetUnset($key) {
    unset($this->files[$key]);
  }

  public function offsetGet($key) {
    return $this->files[$key];
  }

  public function offsetExists($key) {
    return isset($this->files[$key]);
  }

  public function __get($key) {
    return $this->offsetGet($key);
  }

  public function __set($key, $val) {
    $this->offsetSet($key, $val);
  }

  protected $files = array();
}
?>
