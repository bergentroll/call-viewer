<div class='toolbar'>
  <div class='navigation'>
    <a href='/?page=1&amp;<?php echo $filters_link; ?>'>
      <img src='img/beg.svg' alt='начало'/>
    </a>
    <a href='/?page=<?php echo $page - 1; ?>&amp;<?php echo $filters_link; ?>'>
      <img src='img/prev.svg' alt='пред.'/>
    </a>
    <a href='/?page=<?php echo $page ?>&amp;<?php echo $filters_link ?>'>
      <?php echo $page; ?>
    </a>
    <a href='/?page=<?php echo $page + 1; ?>&amp;<?php echo $filters_link; ?>'>
      <img src='img/next.svg' alt='след.'/>
    </a>
    <a href='/?page=<?php echo (int)($files_number / $ITEMS_ON_PAGE + 1) ?>&amp;<?php echo $filters_link ?>'>
      <img src='img/end.svg' alt='конец'/>
    </a>
  </div>

  <form>
    <input type='hidden' name='page' value='<?php echo $page; ?>'>
    <select name='filter_ext' method='GET' onchange='this.form.submit()'>

    <?php
      foreach ($FILTERS_EXT as $label => $value) {
        $line = "<option value='$value'";
        if ($value == $filter_ext) $line .= " selected='select'";
        $line .= ">$label</option>" . PHP_EOL;
        echo $line;
      }
    ?>

    </select>
    <select name='filter_dir' method='GET' onchange='this.form.submit()'>

    <?php
      foreach ($FILTERS_DIR as $label => $value) {
        $line = "<option value='$value'";
        if ($value == $filter_dir) $line .= " selected='select'";
        $line .= ">$label</option>" . PHP_EOL;
        echo $line;
      }
    ?>

    </select>
  </form>
</div>
