<?php

/* call-viewer
 * Copyright (C) 2018 Anton Karmanov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once 'config.php';
require_once 'files-class.php';

// Initialize get variables.
if(isset($_GET['page'])) {
  $page = $_GET['page'];
} 
else {
  $page = 1;
}

if(isset($_GET['filter_ext'])) {
  $filter_ext = $_GET['filter_ext'];
}
else {
  $filter_ext = '';
}

if(isset($_GET['filter_dir'])) {
  $filter_dir = $_GET['filter_dir'];
}
else {
  $filter_dir = '';
}

$filters_link = "filter_ext=${filter_ext}&amp;filter_dir=${filter_dir}";
$filter = "(?=$filter_ext)(?=$filter_dir)";

// Create array of records filenames.
$files = new Files($DIR, $filter, $FILE_SUFFIX);

// Count records.
$files_number = count($files);

// Make toolbar.
require_once "toolbar.php";

// Message for page that is out of range.
if (($page - 1) * $ITEMS_ON_PAGE >= $files_number || $page < 1) {
  echo "<tr><td>Страница не содержит элеметов.</td></tr>";
  goto END;
}

// Iterate page entries.
for ($i = $ITEMS_ON_PAGE * ($page - 1); $i < $ITEMS_ON_PAGE * $page && $i < $files_number; $i++) {
  $file = $files[$i];
  $year = substr($file, 0, 4);
  $mon = substr($file, 4, 2);
  $day = substr($file, 6, 2);
  $H = substr($file, 9, 2);
  $M = substr($file, 11 ,2);
  $S = substr($file, 13 ,2);
  preg_match('/-(\d+)-(\d+)\./', $file, $call_array);
  $call_from = $call_array[1];
  $call_to = $call_array[2];
  $pic = str_replace(".${FILE_SUFFIX}", ".png", $file);

  if (strlen($call_from) == 11) {
    $txt_from = "<a href='$NUMBER_URL$call_from' target='_blank'>$call_from</a>";
  }
  else {
    $txt_from = $call_from;
  }

  if (strlen($call_to) == 11) {
    $txt_to = "<a href='$NUMBER_URL$call_to' target='_blank'>$call_to</a>";
  }
  else {
    $txt_to = $call_to;
  }

  require "cells.php";
}

// Unconditional branch label.
END:
?>
