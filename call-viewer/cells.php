<table class='card'>
  <tr>
    <td class='txt'>
      <?php echo "$day/$mon/$year"; ?><br>
      <?php echo "$H:$M:$S"; ?><br>
      С номера: 
      <?php echo $txt_from ?><br>
      На номер: 
      <?php echo $txt_to ?>
    </td>
    <td class='spectrogram-cell' style='background-image:url(<?php echo "$DIR$pic"; ?>);'></td>
  </tr>
  <tr>
    <td colspan='99'>
      <audio preload='metadata' controls>
        <source src='<?php echo "$DIR/$file"; ?>' type='<?php echo mime_content_type("$DIR/$file") ?>'/>
      </audio>
    </td>
  </tr>
</table>
