<?php

# Variable points to symlink that points to directory with records.
$DIR = "data/";

# How many entries print on a page.
$ITEMS_ON_PAGE = 30;

# Suffix of record files.
$FILE_SUFFIX = 'wav';

### Lists of filters ###
$FILTERS_EXT = [
# Filter name          => RegEx
  'Все записи'         => '',             # Match any;
  'Записи ЦОК'         => '-3\d{2}[\.-]', # Match _3XX extentions;
  'Записи колл-центра' => '-203[\.-]'     # Match the extention 203.
];

$FILTERS_DIR = [
# Filter name       => RegEx
  'Все направления' => '',         # Match any;
  'Входящие'        => '-\d{3}\.', # Match filenames ends with _3XX;
  'Исходящие'       => '-\d{3}-'   # Match filenames with _3XX third group.
];

# Caller numbers and desination numbers will be displayed as links when its
# will be 11 signs length. Number will be appenden to the link.
$NUMBER_URL = 'https://duckduckgo.com/?q=';

?>
